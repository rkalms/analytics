var express = require('express');

var app = express();

app.get('/', function(req, res) { 
	res.sendfile('./app/index.html');
});

express()
  .use(express.vhost('cookietest.kalms.co', app))
  .listen(4040);