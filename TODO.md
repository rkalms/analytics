# Flow:

User sees landingpage.

	-> User logs in.

User is presented with each repository attached to him/her.

	-> User selects repository, and is presented with issue list and available branches to him or her.

Alternatively, the user is presented with a model, and the option of attaching it to a repository -- to achieve or enforce the workflow feature branching gives us.

